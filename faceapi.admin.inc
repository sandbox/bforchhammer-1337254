<?php

/**
 * Configuration form
 */
function faceapi_form($form, &$form_state) {
  
  $apikey = variable_get('faceapi_apikey');
  $apisecret = variable_get('faceapi_apisecret');

  
  $form['faceapi_apikey'] = array(
    '#title' => t('API Key'),
    '#description' => t('Please enter your 24-digit face.com API Key.'),
    '#type' => 'textfield',
    '#default_value' => $apikey,
    '#maglength' => 24,
    '#required' => TRUE,
  );
  
  $form['faceapi_apisecret'] = array(
    '#title' => t('API Secret'),
    '#description' => t('Please enter your 24-digit face.com API Secret.'),
    '#type' => 'textfield',
    '#default_value' => $apisecret,
    '#maglength' => 24,
    '#required' => TRUE,
  );
  
  $authentication_test = false;
  if (!empty($apikey) && !empty($apisecret)) {
    $response = faceapi_api('account_authenticate');
    $authentication_test = $response->status == 'success' && $response->authenticated == TRUE;
    //$limits = faceapi_api('account_limits');
  }
  
  $form['faceapi_status'] = array(
    '#markup' => '<strong>' . t('Face.com Authentication') . ': </strong>' . 
      ($authentication_test ? '<span class="success">successful</span>' : '<span class="error">error</span>'),
  );
  
  return system_settings_form($form);
}