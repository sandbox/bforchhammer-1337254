<?php

/**
 * @file
 */

/**
 * Scale and crop an image to the specified size using GD.
 *
 * @param $image_data An image object.
 * @param $requested_x The requested image width.
 * @param $requested_y The requested image height.
 * @param $upscale TRUE if upscaling is allowed.
 * @return TRUE if successful.
 */
function image_gd_facecrop_scale(stdClass $image_data, $requested_x, $requested_y, $upscale) {
  $ratio = max($requested_x / $image_data->info['width'], $requested_y / $image_data->info['height']);
  if ($ratio <= 1 || $upscale) {
    if (!image_gd_resize($image_data, round($ratio * $image_data->info['width']), round($ratio * $image_data->info['height']))) {
      return FALSE;
    }
  }
  return image_gd_facecrop_crop($image_data, $requested_x, $requested_y);
}

/**
 * Crop an image, removing the lowest entropy areas.
 *
 * @param array $image_data
 * @param int $requested_x
 * @param int $requested_y
 * @return booleon TRUE if successful
 */
function image_gd_facecrop_crop(stdClass $image_data, $requested_x, $requested_y) {

  // public url to source image ("uploading" to webservice does not work yet)
  // using the "source image" instead of the actual image means that face detection
  // might be off, if previous actions change the image dimensions in any way...
  $image_url = file_create_url($image_data->source);
  $image_url = str_replace('http://comteam.dev', 'http://rc.comteam.ag', $image_url);
  
  $faces = faceapi_faces_detect($image_url);
  $faces = $faces[0]; // we only have on image

  if (!empty($faces->tags)) {
    $width = $image_data->info['width'];
    $height = $image_data->info['height'];

    $face_width = $faces->tags[0]->width / 100; // Face Width % of full image. 
    $face_height = $faces->tags[0]->height / 100; // Face Height % of full image.
    $face_center_x = $faces->tags[0]->center->x / 100; // face center X %
    $face_center_y = $faces->tags[0]->center->y / 100; // face center Y %
    
    $face_center_x = $face_center_x + ($face_width/2);
    $face_center_y = $face_center_y - ($face_height/2);
    
    $image = $image_data->resource;
    $dx = imagesx($image) - min(imagesx($image), $requested_x);
    $dy = imagesy($image) - min(imagesy($image), $requested_y);
    $left = round($face_center_x * $dx);
    $right = imagesx($image) - round((1 - $face_center_x) * $dx);
    $top = round($face_center_y * $dy);
    $bottom = imagesy($image) - round((1 - $face_center_y) * $dy);
        
    // Finally, crop the image using the coordinates found above.
    $cropped_image = image_gd_create_tmp($image_data, $right - $left, $bottom - $top);
    imagecopy($cropped_image, $image, 0, 0, $left, $top, $right - $left, $bottom - $top);
    imagedestroy($image_data->resource);
    $image_data->resource = $cropped_image;
    $image_data->info['width'] = $requested_x;
    $image_data->info['height'] = $requested_y;
    return TRUE;
  }
  else {
    // oh no, no faces detected. what now?
    drupal_set_message('No faces found in <code>' . $image_url . '</code>');
    return image_gd_crop($image_data, 0, 0, $requested_x, $requested_y);
  } 
}
